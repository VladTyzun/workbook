<?php
session_start();
if((empty($_SESSION['uid']) 
	|| ($_SESSION['uid'] != sha1(session_id() + date("j")))))
{
	header("Location: http://".$_SERVER['SERVER_NAME']."/adminKA/login.php");
	exit;
}

include("../classes/Books.php");
$books = new Books();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, maximum-scale=2.0" />
	<title>Document</title>
	
<link rel="stylesheet" href="../css/reset.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<link rel="stylesheet" href="../css/style.css">
<link rel="stylesheet" href="../css/style-responsive.css">


</head>
<body>

	<div class="container">
		
		<div class="row-fluid">
	
<div class="overlay"></div>			
							
<div class="toggle-add-book col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8  col-md-offset-2 col-lg-6 col-lg-offset-3">
		
			<div class="close">CLOSE</div>	
		
	<form action="../connector.php" method="POST" enctype="multipart/form-data">		
	<input name="add_isbn" type="text" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" placeholder="ISBN code: *">
	<input name="add_author" type="text" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" placeholder="author *">
	<input name="add_genre" type="text" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" placeholder="genre *"> 
	<input name="add_title" type="text" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" placeholder="book title *">
	<input name="add_price" type="text" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" placeholder="price *">	
	<input name="add_img" type="file" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<input name="add_date" type="date" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" placeholder="date">
	<textarea name="add_description" cols="30" rows="10" placeholder="Description" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></textarea>
	<input type="submit" class="btn btn-success" value="Save">
	</form>	
</div>
			
			
<div class="admin-header col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<h3 class="col-xs-6 col-sm-8 col-md-10 col-lg-10">Hello, Admin</h3>
			
		<button class="add-admin-btn btn btn-info col-xs-6 col-sm-4 col-md-2 col-lg-2">Add Book</button>	
				
			</div>
			
			
			
				
			<div class="admin-content col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
<?php
			$sql ="SELECT * FROM `books` 
					INNER JOIN `author` ON `books`.`id_author` = `author`.`id_author` 
					INNER JOIN `genre` ON `books`.`id_genre` = `genre`.`id_genre` 
					ORDER BY `isbn`";

			$rows = $books->ExecQuery($sql, true);

	 foreach ($rows as $row) {
				if(is_file("../image/mini/{$row['src_img']}"))
						$img_src = "../image/mini/{$row['src_img']}";
					else
						$img_src = "../image/mini/default.png";
echo<<<PRINT
<table class="table table-hover" id='id-{$row['isbn']}''> 
<tr>
<td>
	<img src="{$img_src}" alt="" class="img-responsive col-xs-5 col-sm-4 col-md-2 col-lg-2" alt="Responsive image" />
	<div class='admin-description col-xs-7 col-sm-8 col-md-8 col-lg-8'>
		<input type=text name='u_title_{$row['isbn']}' class='admin-book-title col-xs-12 col-sm-12 col-md-12 col-lg-12' value="{$row['title']}">
		<input type=text name='u_price_{$row['isbn']}' class='admin-book-price col-xs-12 col-sm-12 col-md-12 col-lg-12' value="{$row['price']}"$'>
		<input type=text name='u_author_{$row['isbn']}' class='admin-book-author col-xs-12 col-sm-12 col-md-12 col-lg-12' value='{$row['name']}'>
		<input type=text name='u_genre_{$row['isbn']}' class='admin-book-author col-xs-12 col-sm-12 col-md-12 col-lg-12' value='{$row['genre']}'>
		<input type=text name='u_isbn_{$row['isbn']}' class='admin-book-isbn col-xs-12 col-sm-12 col-md-12 col-lg-12' value='{$row['isbn']}'>
		<input type=text name='u_date_{$row['isbn']}' class='admin-book-title col-xs-12 col-sm-12 col-md-12 col-lg-12' value="{$row['date']}">
		<textarea name='u_desc_{$row['isbn']}' class='admin-book-info col-xs-12 col-sm-12 col-md-12 col-lg-12'>{$row['description']}</textarea>
	</div>
	<div class="buttons col-xs-12 col-sm-12 col-md-2 col-lg-2">
		<button class="btn btn-success" onclick="UpdateItem('{$row['isbn']}')">Save</button>
		<button class="btn btn-danger" onclick="RemoveItem('{$row['isbn']}')">Remove</button>
	</div>
</td>
</tr>
</table>
PRINT;


  }

 echo $books->GetPagination();
?>	
			</div>
		</div>
		
	</div>
	
	<script src="../script/jquery-2.1.1.min.js"></script>
	<script src="../script/bootstrap.min.js"></script>
	<script src="../script/script.js"></script>
	
</body>
</html>