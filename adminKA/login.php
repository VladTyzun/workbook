<?php
session_start();
include("../classes/Login.php");

if($_SERVER['REQUEST_METHOD']=="POST"){

	$login = new Login();
	$result = $login->CheckAdmin($_POST['login'], $_POST['pass']);

	if($result){
		$_SESSION['uid'] = sha1(session_id() + date("j"));
		header("Location: http://".$_SERVER['SERVER_NAME']."/adminKA");		
		exit;
	}
	else{
		$_SESSION['ooops'] = true;
		header("Location: http://".$_SERVER['SERVER_NAME']."/adminKA/login.php");
		exit;
		
		
	}
}

?>

<!DOCTYPE html>
<html>
<head>
<title>Админ-панель</title>
 <meta charset="UTF-8">
 <meta name="MobileOptimized" content="320">
 <meta name="viewport" content="width=device-width, maximum-scale=2.0" />
 
<link rel="stylesheet" href="../css/reset.css">
<link rel="stylesheet" href="../css/style.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">
 
</head>
<body class=login-body>

<div class="container">
	
	<div class="row-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12">

<form method="post" class="login-form form-horizontal col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3" role="form">

	
	 <h1>Admin Login Form:</h1>
	<div class="form-group">
  <div class="col-sm-12">
  <input type="text" name="login" placeholder="Login*" class="form-control" id="inputLogin3">
    </div>
	</div>
	
		
		
		<div class="form-group">
  <div class="col-sm-12">
  <input type="password" name="pass" placeholder="Password*" class="form-control" id="inputPassword3">
    </div>
	</div>	

	
	<div class="form-group">
		
		 <div class="col-sm-2">
      <input class="login-btn btn btn-default" type="submit" value="LOGIN">
    </div>
	</div>
	
	
	<script src="../script/jquery-2.1.1.min.js"></script>
<script src="../script/bootstrap.min.js"></script>
	
<?php
	if(!empty($_SESSION['ooops']))
//		echo "<p>Error!</p>";
//	exit;
		
	?>
	


</form>

</div>


</div>



</body>
</html>
