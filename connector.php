<?php
session_start();

include("classes/Books.php");
$books = new Books();



// add to session
if(!empty($_POST['add_session_isbn'])){
	$isbn = $_POST['add_session_isbn'];

	if(empty($_SESSION['card'][$isbn]))	$_SESSION['card'][$isbn]=1;
	else $_SESSION['card'][$isbn]++;

	exit;
}




// delete session all
if(!empty($_POST['delete_session_all'])){
	session_unset();
}




//delete 1 item
if(!empty($_POST['delete_session_item'])){
	unset($_SESSION['card'][$_POST['delete_session_item']]);
	echo $_POST['delete_session_item'];
	exit;
}




// show card box
if(!empty($_POST['card_box']) && $_POST['card_box']=="show")
{
	
	if(empty($_SESSION['card'])) exit;

	$where = "WHERE `books`.`isbn` IN(";
		foreach ($_SESSION['card'] as $key => $value) {
			$where.="'"; $where.=$key; $where.="'"; $where.=", ";
		}
		$where = substr($where, 0, strlen($where) - 2);
		$where.=")";

$sql = "SELECT * FROM `books` JOIN `author` ON `books`.`id_author` = `author`.`id_author` JOIN `genre` ON `books`.`id_genre` = `genre`.`id_genre` {$where}";
$rows = $books->ExecQuery($sql);

$print="";
foreach ($rows as $row) {

	if(is_file("image/mini/{$row['src_img']}"))
		$img_src = "image/mini/{$row['src_img']}";
	else
		$img_src = "image/mini/default.png";

	$print.="<div id='id-{$row['isbn']}' class='card-books row-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
	$print.="<img src='{$img_src}' alt='' class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>";

	$print.="<div class='description col-xs-10 col-sm-10 col-md-10 col-lg-10'>";
	$print.="<h4 class='title'>{$row['title']}</h4>";
	$print.="<span class='price'>{$row['price']} $</span>";

	$print.="<span class='author'>{$row['name']}</span>";
	$print.="<p class='isbn'>ISBN code: {$row['isbn']}</p>";
	$print.="<button type='button' onclick='delete_session_item(\"{$row['isbn']}\")' class='remove-btn btn btn-danger'>Remove</button>";
	$print.="</div></div>";
}
echo $print;
exit;
}




//update book
if(!empty($_POST['update_item'])){

	$isbn = $_POST['update_item'];
	$out_isbn = $_POST['u_isbn'];
	$title = $_POST['u_title'];
	$price = $_POST['u_price'];
	$author = $_POST['u_author'];
	$date = $_POST['u_date'];
	$desc = $_POST['u_desc'];
	$genre = $_POST['u_genre'];
	

	$result = $books->UpdateItem($isbn, $out_isbn, $title, $author, $genre, $price, $date, $desc);
	if($result)echo true;
	
	exit;
}




// delete book
if(!empty($_POST['del_item'])){
	$result = $books->DeleteItem($_POST['del_item']);
	if($result)echo $_POST['del_item'];
	exit;
}




// create book
if(!empty($_POST['add_title']) && !empty($_POST['add_price'])
	&& !empty($_POST['add_author']) && !empty($_POST['add_isbn']) && !empty($_POST['add_genre'])){

	$isbn = $_POST['add_isbn'];
$title = $_POST['add_title'];
$author = $_POST['add_author'];
$price = $_POST['add_price'];
$genre = $_POST['add_genre'];
$desc = $_POST['add_description'];
$date = (!empty($_POST['add_date'])) ? $_POST['add_date'] : date("Y/m/d") ;
$img_name = NULL;


if(!empty($_FILES['add_img']['name'])){
	$imageinfo = getimagesize($_FILES['add_img']['tmp_name']);
	if($imageinfo['mime'] != 'image/gif' && $imageinfo['mime'] != 'image/jpeg' && $imageinfo['mime'] != 'image/png') {
		echo "Только gif, png, jpeg";
		exit;
	}
	if($_FILES["add_img"]["size"] > 1024*1024*2)
	{
		echo ("Something is wrong... big size");
		exit;
	}

	$img_name = time().$_FILES['add_img']['name'];
	$uploadfile = "image/mini/{$img_name}";


	if (!move_uploaded_file($_FILES['add_img']['tmp_name'], $uploadfile)) {
		echo "Something is wrong... file not loaded";
		exit;
	}
}

$res = $books->InsertItem($isbn, $title, $author, $genre, $price, $date, $img_name, $desc);
if(!$res) {
	echo "Error add item";
}
else header("Location: http://".$_SERVER['SERVER_NAME']."/adminKA/index.php");
exit;
}


echo "Something is wrong...";
?>