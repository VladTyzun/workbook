function add_to_session(data){
	$.ajax({
   type: "POST",
   url: "connector.php",
   data: "add_session_isbn="+data,
 });
}

function delete_session_all(){
	$.ajax({
   type: "POST",
   url: "connector.php",
   data: "delete_session_all=1",
 });
}

function delete_session_item(data){
	$.ajax({
   type: "POST",
   url: "connector.php",
   data: "delete_session_item="+data,
   success: function(data){
		    if (data) $("#id-"+data).fadeOut();
		    else alert("Something is wrong...");         
		  }
 });
}

function show_card_box(){
	$.ajax({
   type: "POST",
   url: "connector.php",
   data: "card_box=show",
   success: function(data){
   	$('.card-info-books').html("");

    if (data)$('.card-info-books').append(data);  
	else $('.card-info-books').append("CARD IS EMPTY");
  }
 });
}

function UpdateItem(data){

	var isbn = $( "input[name='u_isbn_"+data+"']" ).val();
	var title = $( "input[name='u_title_"+data+"']" ).val();
	var price = $( "input[name='u_price_"+data+"']" ).val();
	var author = $( "input[name='u_author_"+data+"']" ).val();
	var date = $( "input[name='u_date_"+data+"']" ).val();
	var genre = $( "input[name='u_genre_"+data+"']" ).val();
	var desc = (!!($( "textarea[name='u_desc_"+data+"']" ).val()))?$( "textarea[name='u_desc_"+data+"']" ).val():" ";//проверка на undefined

	$.ajax({
   type: "POST",
   url: "../connector.php",
   data: "update_item="+data+
   		"&u_isbn="+isbn+
   		"&u_title="+title+
   		"&u_price="+price+
   		"&u_author="+author+
   		"&u_date="+date+
   		"&u_desc="+desc+
   		"&u_genre="+genre,
   success: function(data){
	    if (data) alert("Save success!");
	    else alert("Something is wrong...");         
	  }
	});
}

function RemoveItem(data){
	$.ajax({
	   type: "POST",
	   url: "../connector.php",
	   data: "del_item="+data,
	   success: function(data){
		    if (data) $("#id-"+data).fadeOut();
		    else alert("Something is wrong...");         
		  }
 	});
}

$(document).ready(function(){

   /*---------HEADER FIXED---------*/
	
$header = $("header");  
$scroll_height = 10;
$screen_width = 600;	
	
	
	
$(window).scroll(function(){
	
		  
  if ( $(this).scrollTop() > $scroll_height  && $header.hasClass("default") && $(window).width() >= $screen_width ) 
  			{     $header.removeClass("default").addClass("fixed");
$("body").css("padding-top", $header.height());
            }
	
else if($(this).scrollTop() < $scroll_height && $header.hasClass("fixed") && $(window).width() <= $screen_width ) 
		{  
$header.removeClass("fixed").addClass("default");	
$("body").css("padding-top", 0);
			 
            }
        });//scroll
	


$(window).resize(function(){
		  
  if ($(window).width() >= $screen_width ) 
  			{     $header.removeClass("default").addClass("fixed");		 
$("body").css("padding-top", $header.height());
            } 
else if($(window).width() <= $screen_width) 
			{   
			$header.removeClass("fixed").addClass("default");
			 $("body").css("padding-top", 0);
            }
        });//width
	
/*---------HEADER FIXED---------*/
	
	
	
	//----------SEARCH CATEGORY-------------
	

$("[class^=droping]").click(function(){
	
$(this).parent().find("ul").toggle();									 
});
	
$(".refresh").click(function(){
$(this).parent().parent().find("input[type=checkbox]").attr( "checked", false );		
});//-----refresh own checkbox
	
	$(".refresh-all").click(function(){
$(this).parents().find("input[type=checkbox]").attr( "checked", false );		
});//------refresh all checkboxes
	
$(".dropdown #genre").click(function(){
$(".category-search #genre").css("display","inline-block").show();
});
$(".dropdown #year").click(function(){
$(".category-search #year").css("display","inline-block").show();
});
	
$(".dropdown #author").click(function(){
$(".category-search #author").css("display","inline-block").show();
});
	
if($(".category-search .active").is(":visible")){
	$(".category-search li button").removeClass("disabled");
}
//	else{
//		$(".category-search li button").addClass("btn-default");	
//	}
	
	
//----------SEARCH CATEGORY-------------
	
	

	
	
	
//----------CARD OPEN + COUNTER BOOKS-------------	

	
	

$(".open-card").click(function(){	
$(".overlay,.card-toggle").show();
	
	if($(".card-toggle").is(":visible")){
		
		$("body").css("overflow","hidden");
	}
	
});

$(".close").click(function(){
$(".overlay,.card-toggle").hide();
	
if($(".card-toggle").is(":hidden")){
	
$("body").css("overflow","scroll");
	}
		
	
}); //------- open card

	
	
	
$counter=0;


$(".add-button").click(function(){	
$(".count2").fadeIn().delay(1000).text("+"+($counter+1)).fadeOut();
//$counter++;
});//------ counter
	
$(".remove-btn").click(function(){	
$(this).parents(".card-books").hide();	
//$(".count2").text("+"+($counter));		
//$counter--;
	});


$(".remove-all").click(function(){	
$(".card-books").hide();
$(".card-info-books").text("CARD IS EMPTY");
});
	
	
	
	
	
	
//-------------admin----------

$(".add-admin-btn").click(function(){

$(".toggle-add-book, .overlay").show();
		
});	
	
$(".close").click(function(){

$(".toggle-add-book, .overlay").hide();
		
});	
		
	
$(".open-img").click(function(){

	
	
});	
		
	
});


















