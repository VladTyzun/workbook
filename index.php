<?php include("header.php");?>

<section class="p-t container">

 <div class="row-fluid">

  <div class="container-1">

   <div class=' books row-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12'>

   <?php //WHILE START  
   $sql = "SELECT * FROM `books`
   JOIN `author` ON `books`.`id_author` = `author`.`id_author`
   JOIN `genre` ON `books`.`id_genre` = `genre`.`id_genre`";

   if(!empty($_POST['search_text']) || !empty($_POST['c_year']) || !empty($_POST['c_genre']) || !empty($_POST['c_author'])){
      include("classes/SearchBooks.php");
      $search = new SearchBooks();
      $sql .= $search->GetWhereSQL($_POST['c_genre'], $_POST['c_author'], $_POST['c_year'], $_POST['search_text']);
   }

   $rows = $books->ExecQuery($sql, true);
   if(empty($rows))echo "<h3>Books not found</h3>";
   foreach ($rows as $row) {

    if(is_file("image/mini/{$row['src_img']}"))
      $img_src = "image/mini/{$row['src_img']}";
    else
      $img_src = "image/mini/default.png";

    echo<<<PRINT

    <div class=' book row-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12 '>  


      <img src="{$img_src}" alt='' class='img-responsive col-xs-5 col-sm-4 col-md-3 col-lg-2' alt='Responsive image' />

      <div class='description col-xs-7 col-sm-8 col-md-9 col-lg-10'>
        <h4 class='title'>{$row['title']}</h4>
        <span class='price'>{$row['price']} $</span>
        <span class='author'>{$row['name']}</span>
        <span class='genre'>{$row['genre']}</span>
        <span class='date'>{$row['date']}</span>
        <p class='isbn'>ISBN code: {$row['isbn']}</p>
        <p class='text-info'>{$row['description']}</p>

        <button type='button' onclick='add_to_session("{$row['isbn']}");' class='add-button success btn btn-success col-xs-12 col-sm-3 col-md-2 col-lg-2'>Add to Card</button>
      </div>


    </div>

PRINT;
  }
//END WHILE


  ?>
</div>
</div>

</div>


<div class=" page row-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12">
 <span class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
   <?php echo $books->GetPagination(); ?>
 </span> 
</div>


</section>

<?php include("footer.php"); ?>