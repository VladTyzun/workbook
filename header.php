<?php 
session_start();
include("classes/Books.php");
$books = new Books();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, maximum-scale=2.0" />
  <title>Document</title>

  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/style-responsive.css">

<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->




<!--[if IE8]>
<script>
  
  window.location("404.php");
  
</script>
<![endif]-->

</head>
<body>

  <div class="container">
    

   <div class="card-toggle">
    <?php include('card.php'); ?>
  </div>


</div>

<div class="overlay"></div>


<header class="default">
  <div class="container">


   <div class="row-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12">


     <div class="logo  col-xs-12 col-sm-3 col-md-3 col-lg-2">
       <a href="index.php"><h2 title="BookCase — Store">Book<span>Case</span></h2></a>
       <span class="moto">reading is knouladge</span>

       <div class="shopcard">
        <h6 class="open-card" onclick="show_card_box();">YOUR CARD</h6>
        <span class="count2"></span>
      </div>

    </div>  

    <div class="search-group col-xs-12 col-sm-8 col-sm-offset-1 col-md-6 col-md-offset-3 col-lg-5 col-lg-offset-5">
      <form action="" method="POST" name="search">
        <div class="input-group">
         <input type="text" name="search_text" class="form-control" placeholder="search book you need"  autocomplete="off">

         <div class="style-button input-group-btn">
           <button type="submit" class="btn btn-default ">
            Search
          </button>

    </div><!-- /btn-group -->
  </div><!-- /input-group -->

  <ol class="category-search breadcrumb col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <li class="title visible">Serch by: </li>

    <li class="active" id='genre'>

     <!-- Split button -->
     <div class="btn-group">
      <button type="button" class="btn btn-xs btn-default">Genre</button>
      <button type="button" class="droping btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
      </button>
      <ul class="dropdown-menu" role="menu">

        <?php
        $sql = "SELECT * FROM `genre`";
        $rows = $books->ExecQuery($sql);
        foreach ($rows as $row)
          echo "<li><input type='checkbox' name='c_genre[]' value='{$row['id_genre']}'>{$row['genre']}</li>"; 
        ?>

        <li class="divider"></li>
        <li>
          <button type="button" class="refresh btn btn-xs btn-default">Refresh</button>
        </li>
      </ul>
    </div>


  </li>

  <li class="active" id='author'>  
   <!-- Split button -->
   <div class="btn-group">
    <button type="button" class="btn btn-xs btn-default">Author</button>
    <button type="button" class="droping btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
      <span class="caret"></span>
      <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
     <?php
     $sql = "SELECT * FROM `author`";
     $rows = $books->ExecQuery($sql);
     foreach ($rows as $row)
      echo "<li><input type='checkbox' name='c_author[]' value='{$row['id_author']}'>{$row['name']}</li>";
    ?>
    <li class="divider"></li>
    <li>  
      <button type="button" class="refresh btn btn-xs btn-default">Refresh</button>
    </li>
  </ul>
</div>

</li>

<li class="active" id='year'>  
 <!-- Split button -->
 <div class="btn-group">
  <button type="button" class="btn btn-xs btn-default">Year</button>
  <button type="button" class="droping btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <?php
    $sql = "SELECT DISTINCT YEAR(`date`) as `year` FROM `books` ORDER BY `date` DESC";
    $rows = $books->ExecQuery($sql);
    foreach ($rows as $row)
      echo "<li><input type='checkbox' name='c_year[]' value='{$row['year']}'>{$row['year']}</li>";

    ?>
    <li class="divider"></li>
    <li>
     <button type="button" class="refresh btn btn-xs btn-default">Refresh</button>
   </li>
 </ul>
</div>

</li>


<li class="visible">
 <button type="button" class="refresh-all btn btn-default btn-xs disabled">refresh</button>
</li>
</ol>

</form>
</div><!-- /.col-lg5-->

</div>


<div class="row-fluid">

  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">


  </div>
</div>

</div>
</header>