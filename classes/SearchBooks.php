<?php

class SearchBooks{


	public function GetWhereSQL($ar_genre, $ar_author, $ar_year, $search_text){

		if(empty($ar_genre) && empty($ar_author) && empty($ar_year)  && empty($search_text)) return " "; 

		$where = " WHERE ";

		if(!empty($search_text)){
			$where.= "`books`.`description` LIKE '%{$search_text}%'
					OR `books`.`title` LIKE '%{$search_text}%' 
					OR `author`.`name` LIKE '%{$search_text}%'
					OR `genre`.`genre` LIKE '%{$search_text}%'
					OR `books`.`date` LIKE '%{$search_text}%'
					OR `books`.`price` LIKE '%{$search_text}%'
					OR `books`.`isbn` LIKE '%{$search_text}%' ";
			if(!empty($ar_genre) || !empty($ar_author) || !empty($ar_year)) $where .= "AND ";
		}

		if(!empty($ar_genre)){
			$where.=" `books`.`id_genre` IN (";
			foreach ($ar_genre as $id_genre) {
				$where.="'"; 
				$where.= $id_genre;
				$where.="'";
				$where.=", ";
			}
			$where = substr($where, 0, strlen($where) - 2);
			$where .= (!empty($ar_author) || !empty($ar_year))? ") AND ": ")";
		}

		if(!empty($ar_author)){
			$where.="`books`.`id_author` IN (";
			foreach ($ar_author as $id_author) {
				$where.="'"; 
				$where.= $id_author;
				$where.="'";
				$where.=", ";
			}
			$where = substr($where, 0, strlen($where) - 2);
			$where .= (!empty($ar_year))? ") AND ": ")";
		}

		if(!empty($ar_year)){
			$where.="YEAR(`date`) IN (";
			foreach ($ar_year as $year) {
				$where.="'"; 
				$where.= $year;
				$where.="'";
				$where.=", ";
			}
			$where = substr($where, 0, strlen($where) - 2);
			$where .= ")";
		}

		return $where;
	}

}
?>