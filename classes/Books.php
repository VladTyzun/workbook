<?php
include("DB.php");

class Books extends DB{

	private $dbh;

	public $CountPages;
	public $CurPage;

	function __construct(){
		parent::__construct();
		$this->dbh = parent::GetDBH();
	}

	public function ExecQuery($sql, $pagination=false, $perPage = 10){

		if($pagination){
			if($perPage<=0)$perPage=10;
			$sth = $this->dbh->prepare($sql);
			$sth->execute();
			// количество строк
			$total_rows=$sth->rowCount();
			// количество страниц
			$count_pages=ceil($total_rows/$perPage);

			// получаем номер страницы
			$page=0;
			if(isset($_GET['page'])) $page=(int)($_GET['page']-1);
			// для удобства пользователя <0 = 0 ; >0 = num page
			if ($page<0) $page=0;
			if($page>=$count_pages) $page=$count_pages-1; 

			// вычисляем первый оператор для LIMIT
			$start=abs($page*$perPage);
			$sql.=" LIMIT {$start},{$perPage}";

			$this->CountPages = $count_pages;
			$this->CurPage = $page;
		}

		$sth = $this->dbh->prepare($sql);
		$sth->execute();
		return $sth->fetchAll(PDO::FETCH_ASSOC);
	}

	public function GetPagination(){
		
		$PrintPagin = "<ul class='pagination'>";


		if($this->CurPage>0) $PrintPagin .= "<li><a href='".$_SERVER['PHP_SELF']."?page=".$this->CurPage."'>&laquo;</a></li>";

		for($i=1;$i<=$this->CountPages;$i++) {

			if($this->CountPages==1)continue;

			if($num_pages>9 && $i>3 && $i<($this->CountPages-2)) {
				if($this->CurPage>2){
					if($this->CurPage>3)$PrintPagin .= "<li><a href='javascript:void(0);'>...</a></li>";
					if($this->CurPage<($this->CountPages-3))$PrintPagin .= "<li><a href='javascript:void(0);' style='color:red;'>".($this->CurPage+1)."</a></li>";
				}
				if($this->CurPage<($this->CountPages-4))$PrintPagin .= "<li><a href='javascript:void(0);'>...</a></li>";
				$i=$this->CountPages-2;
			}

				// текущая страница
			if ($i-1 == $this->CurPage) {  	
				$PrintPagin .= "<li><a href='javascript:void(0);' style='color:red;'>{$i}</a></li>";
			} else {
				$PrintPagin .= "<li><a href=".$_SERVER['PHP_SELF']."?page={$i}>{$i}</a></li>";
			}

		}

		if(($this->CurPage+2)<=$this->CountPages)  $PrintPagin .= "<li><a href='".$_SERVER['PHP_SELF']."?page=".($this->CurPage+2)."'>&raquo;</a></li>";

		$PrintPagin .= "</ul>";

		return $PrintPagin;
	}

	public function DeleteItem($isbn){
		$sql = "DELETE FROM `books` WHERE `isbn` = :isbn";
		$sth = $this->dbh->prepare($sql);
		$sth->execute(array(':isbn' => $isbn));
		if($sth->rowCount()>0) return true;
		else return false;
	}

	public function UpdateItem($in_isbn,$out_isbn, $title, $author, $genre, $price, $date, $description){
		$sql = "UPDATE
		    `books`, `author`, `genre`
		SET
		    `author`.`name` = :author,
		    `genre`.`genre` = :genre,
		    `books`.`isbn` = :out_isbn,
		    `books`.`title` = :title,
		    `books`.`price` = :price,
		    `books`.`date` = :datePub,
		    `books`.`description` = :description
		WHERE
		    `books`.`id_author` = `author`.`id_author` AND
		    `books`.`id_genre` = `genre`.`id_genre` AND
		    `books`.`isbn` = :in_isbn";

		$sth = $this->dbh->prepare($sql);			
		$result = $sth->execute(array(':in_isbn' => $in_isbn, ':author' => $author, ':genre' => $genre,
								':title' => $title, ':datePub' => $date, ':out_isbn' => $out_isbn,
								':description' => $description, ':price' => $price));

		if(!$result) return "Error update item";
		return true;
		

	}

	public function InsertItem($isbn, $title, $author, $genre, $price, 
								$date=NULL, $image="default.png", $description = "Good book.")
	{

		$id_author = $this->AddAuthor($author);
		$id_genre = $this->AddGenre($genre);

 		if(!$id_author) return "Error add author";
		if(!$id_genre) return "Error add genre";

		$sql = "INSERT INTO `books` (`isbn`, `id_author`, `id_genre`, `title`, `date`, `src_img`, `price`, `description`) 
		VALUES (:isbn, :id_author, :id_genre, :title, :datePub, :image, :price, :description)";

		$sth = $this->dbh->prepare($sql);			
		$result = $sth->execute(array(':isbn' => $isbn, ':id_author' => $id_author, ':id_genre' => $id_genre,
								':title' => $title, ':datePub' => $date, ':image' => $image, 
								':description' => $description, ':price' => $price));

		if(!$result) return "Error add item";
		return true;
	}


	public function AddAuthor($author){
		$id_author;

		// проверка на наличие автора
		$sql = "SELECT * FROM `author` WHERE `name` = :author";
		$sth = $this->dbh->prepare($sql);
		$sth->execute(array(':author' => $author));

		if($sth->rowCount()>0){
			$result = $sth->fetch(PDO::FETCH_ASSOC);
			$id_author = $result['id_author'];
		}else{
			// добавление нового автора
			$sql = "INSERT INTO `author` (`id_author`, `name`) VALUES (NULL, :name)";
			$sth = $this->dbh->prepare($sql);
			
			if($sth->execute(array(':name' => $author))){
				$sql = "SELECT * FROM `author` WHERE `name` = :author";
				$sth = $this->dbh->prepare($sql);
				$sth->execute(array(':author' => $author));
				$temp = $sth->fetch(PDO::FETCH_ASSOC);
				$id_author = $temp['id_author'];
			}else
				return false;
		}
		return $id_author;
	}

	public function AddGenre($genre){
		$id_genre;

		// проверка на наличие жанра
		$sql = "SELECT * FROM `genre` WHERE `genre` = :genre";
		$sth = $this->dbh->prepare($sql);
		$sth->execute(array(':genre' => $genre));

		if($sth->rowCount()>0){
			$result = $sth->fetch(PDO::FETCH_ASSOC);
			$id_genre = $result['id_genre'];
		}else{
			// добавление нового жанра
			$sql = "INSERT INTO `genre` (`id_genre`, `genre`) VALUES (NULL, :genre)";
			$sth = $this->dbh->prepare($sql);
			
			if($sth->execute(array(':genre' => $genre))){
				$sql = "SELECT * FROM `genre` WHERE `genre` = :genre";
				$sth = $this->dbh->prepare($sql);
				$sth->execute(array(':genre' => $genre));
				$temp = $sth->fetch(PDO::FETCH_ASSOC);
				$id_genre = $temp['id_genre'];
			}else
				return false;
		}
		return $id_genre;
	}

	function GetDBH(){	return parent::GetDBH(); }
}
?>