<?php
class DB{
	
	private $dbh;
	private $db_host ="localhost";
	private $db_user ="root";
	private $db_pass ="";
	private $db_dbname ="work-books";

	function __construct(){
		try {			
			//поключение к базе
			$this->dbh = new PDO("mysql:dbname={$this->db_dbname};host={$this->db_host}", $this->db_user, $this->db_pass);
			} catch (PDOException $e) {
				echo 'Connection failed!';
				exit;
			}
	}
	
	function GetDBH(){	return $this->dbh;	}			
}

?>